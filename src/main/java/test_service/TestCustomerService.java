/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Sabusi
 */
public class TestCustomerService {
     public static void main(String[] args) {
         CustomerService cs = new CustomerService();
         for(Customer customer : cs.getCustomer()) {
             System.out.println(customer);
         }
         
         Customer cus1 = new Customer("Kob", "0909999999");
         cs.addNew(cus1);
         for(Customer customer : cs.getCustomer()) {
             System.out.println(customer);
         }
         
         Customer delCus = cs.getByTel("0909999999");
         delCus.setTel("0909999991");
         cs.update(delCus);
                  for(Customer customer : cs.getCustomer()) {
             System.out.println(customer);
         }
         cs.delete(delCus);
         for(Customer customer : cs.getCustomer()) {
             System.out.println(customer);
         }
    }
}


